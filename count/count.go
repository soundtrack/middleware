package count

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Count struct {
	Id      string `json:"id" bson:"_id,omitempty"`
	Counter int    `json:"counter"`
}

func UpdateFlag() {
	data := Count{}
	session, _ := mgo.Dial("database")
	defer session.Close()
	counter_collection := session.DB("middleware").C("count")
	change := mgo.Change{
		Update:    bson.M{"$inc": bson.M{"counter": 1}},
		ReturnNew: true,
	}
	_, err := counter_collection.Find(bson.M{"_id": "counter"}).Apply(change, &data)
	if err != nil {
		log.Infoln(err)
	}
	log.Infoln("Counter Data: ", data.Counter)
}

func IsFull() bool {
	data := Count{}
	session, _ := mgo.Dial("database")
	defer session.Close()
	counter_collection := session.DB("middleware").C("count")
	err := counter_collection.Find(bson.M{"_id": "counter"}).One(&data)
	if err != nil {
		log.Infoln(err)
	}
	return data.Counter >= 3
}
