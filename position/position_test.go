package position

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCreatePositionStruct(t *testing.T) {
	p := Position{
		X: 5.00,
		Y: 6.00,
	}

	assert.Equal(t, p.X, 5.00)
}
