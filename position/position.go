package position

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2"
)

type Position struct {
	X  float64 `json:"X"`
	Y  float64 `json:"Y"`
	Id string  `json:"id,omitempty"`
}

func NewPosition(X float64, Y float64) *Position {
	return &Position{
		X: X,
		Y: Y,
	}
}

func (position *Position) ToMongo() {
	session, err := mgo.Dial("database")
	if err != nil {
		log.Errorln(err)
	}
	defer session.Close()
	c := session.DB("middleware").C("sound")
	err = c.Insert(position)
	if err != nil {
		log.Errorln(err)
	}
	log.Infoln("[]ToMongo: ", position)
}
