package maths

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/soundtrack/middleware/beacon"
	"gitlab.com/soundtrack/middleware/position"
	"gitlab.com/soundtrack/middleware/stream"
	"gopkg.in/mgo.v2"
	"sort"
)

// ByIdStream implements sort.Interface for []stream.Stream based on
// the Id field.
type ByIdStream []stream.Stream

func (a ByIdStream) Len() int               { return len(a) }
func (a ByIdStream) Swap(i int, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByIdStream) Less(i int, j int) bool { return a[i].Id < a[j].Id }

// ByIdStream implements sort.Interface for []beacon.Beacon based on
// the Id field.
type ByIdBeacon []beacon.Beacon

func (a ByIdBeacon) Len() int               { return len(a) }
func (a ByIdBeacon) Swap(i int, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByIdBeacon) Less(i int, j int) bool { return a[i].Id < a[j].Id }

// vitesse in cm/us
var vitesse float64 = 0.034

func GetEventPosition() {

	sound := position.Position{}
	session, err := mgo.Dial("database")
	if err != nil {
		log.Errorln(err)
	}
	defer session.Close()
	streams := GetStreams(session)
	delta := GetDelta(streams)

	fire_collection := session.DB("middleware").C("fire")
	sound = position.Position{
		X:  delta[0].X,
		Y:  delta[1].Y,
		Id: "Fire",
	}
	log.Infoln("Sound detected at:", sound)
	err = fire_collection.Insert(sound)
	if err != nil {
		log.Errorln(err)
	}
}

func GetStreams(session *mgo.Session) []stream.Stream {
	var stream_array []stream.Stream
	stream_collection := session.DB("middleware").C("stream")
	err := stream_collection.Find(nil).All(&stream_array)
	if err != nil {
		log.Println(err)
	} else {
		log.Infoln("Streams fetched from db: ", stream_array)
		return stream_array
	}
	return nil
}

func GetBeacons(session *mgo.Session) []beacon.Beacon {
	var beacon_array []beacon.Beacon
	beacon_collection := session.DB("middleware").C("beacon")
	err := beacon_collection.Find(nil).All(&beacon_array)
	if err != nil {
		log.Println(err)
	} else {
		log.Infoln("Beacons fetched from db: ", beacon_array)
		return beacon_array
	}
	return nil
}

func GetDistanceBetweenBeacons(beacon_array []beacon.Beacon) []position.Position {
	var distance_array []position.Position

	/* beacon_2 - beacon_1 and beacon_3 - beacon_2
	beacon_3
	.
	.
	.
	.
	beacon_2 . . . . . . .beacon_1
	*/
	sort.Sort(ByIdBeacon(beacon_array))
	distance_1 := beacon_array[1].Position.X - beacon_array[0].Position.X
	distance_2 := beacon_array[2].Position.Y - beacon_array[1].Position.Y
	log.Infoln("Distance 1, Distance 2: ", distance_1, distance_2)
	distance_array = append(
		distance_array,
		position.Position{
			X:  distance_1,
			Y:  0.00,
			Id: "distance_1",
		},
	)
	distance_array = append(
		distance_array,
		position.Position{
			X:  0.00,
			Y:  distance_2,
			Id: "distance_2",
		},
	)
	return distance_array
}

func GetDelta(stream_array []stream.Stream) []position.Position {
	var stream_delta []position.Position

	/* t2 - t1 and t3 - t2 */
	sort.Sort(ByIdStream(stream_array))
	delta1 := stream_array[1].Timestamp - stream_array[0].Timestamp
	delta2 := stream_array[2].Timestamp - stream_array[1].Timestamp
	log.Infoln("Delta 1, Delta 2: ", delta1, delta2)
	stream_delta = append(
		stream_delta,
		position.Position{
			X:  delta1 * vitesse,
			Y:  0.00,
			Id: "delta_1",
		},
	)
	stream_delta = append(
		stream_delta,
		position.Position{
			X:  0.00,
			Y:  delta2 * vitesse,
			Id: "delta_2",
		},
	)
	return stream_delta
}
