package maths

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/soundtrack/middleware/beacon"
	"gitlab.com/soundtrack/middleware/position"
	"gitlab.com/soundtrack/middleware/stream"
	"sort"
	"testing"
)

func TestGetStreamSortedById(t *testing.T) {
	stream_array := []stream.Stream{}
	stream_array = append(
		stream_array,
		stream.Stream{
			Id:        "beacon_2",
			Timestamp: 1234,
		},
	)
	stream_array = append(
		stream_array,
		stream.Stream{
			Id:        "beacon_3",
			Timestamp: 1234,
		},
	)
	stream_array = append(
		stream_array,
		stream.Stream{
			Id:        "beacon_1",
			Timestamp: 1234,
		},
	)
	sort.Sort(ByIdStream(stream_array))
	assert.Equal(t, stream_array[0].Id, "beacon_1")
}

func TestGetDeltaTime(t *testing.T) {
	var stream_delta []position.Position
	stream_array := []stream.Stream{}
	stream_array = append(
		stream_array,
		stream.Stream{
			Id:        "beacon_2",
			Timestamp: 3,
		},
	)
	stream_array = append(
		stream_array,
		stream.Stream{
			Id:        "beacon_3",
			Timestamp: 10,
		},
	)
	stream_array = append(
		stream_array,
		stream.Stream{
			Id:        "beacon_1",
			Timestamp: 1,
		},
	)
	stream_delta = GetDelta(stream_array)

	assert.InDelta(t, stream_delta[0].X, 0.068, 0.001)
	assert.InDelta(t, stream_delta[1].Y, 0.238, 0.001)
}

func TestGetDistanceBetweenBeacons(t *testing.T) {
	var distance_array []position.Position
	beacon_array := []beacon.Beacon{}
	beacon_array = append(
		beacon_array,
		beacon.Beacon{
			Id: "beacon_2",
			Position: position.Position{
				X: 0.00,
				Y: 0.00,
			},
		},
	)
	beacon_array = append(
		beacon_array,
		beacon.Beacon{
			Id: "beacon_3",
			Position: position.Position{
				X: 0.00,
				Y: 2.00,
			},
		},
	)
	beacon_array = append(
		beacon_array,
		beacon.Beacon{
			Id: "beacon_1",
			Position: position.Position{
				X: 5.00,
				Y: 0.00,
			},
		},
	)
	distance_array = GetDistanceBetweenBeacons(beacon_array)

	assert.Equal(t, distance_array[0].X, -5.00)
	assert.Equal(t, distance_array[1].Y, 2.00)
}
