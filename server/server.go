package server

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/soundtrack/middleware/client"
	"gitlab.com/soundtrack/middleware/count"
	"gopkg.in/mgo.v2"
	"net"
)

type Server struct {
	Port string
}

func (self *Server) Start() {
	n := 0
	ln, _ := net.Listen("tcp", self.Port)
	session, err := mgo.Dial("database")
	if err != nil {
		log.Panicln(err)
	}
	defer session.Close()
	counter_collection := session.DB("middleware").C("count")
	err = counter_collection.Insert(&count.Count{Id: "counter", Counter: 0})
	log.Infoln("Counter created")
	for {
		conn, _ := ln.Accept()
		n++
		go client.NewClient(conn)
		log.Infoln("[OK] New beacon, total: ", n)
	}
}
