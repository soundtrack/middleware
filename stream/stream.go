package stream

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/soundtrack/middleware/count"
	"gopkg.in/mgo.v2"
)

type Stream struct {
	Id        string  `json:"id"`
	Timestamp float64 `json:"timestamp"`
}

func (stream *Stream) ToMongo() {
	session, err := mgo.Dial("database")
	if err != nil {
		log.Errorln(err)
	}
	defer session.Close()
	stream_collection := session.DB("middleware").C("stream")
	err = stream_collection.Insert(stream)
	if err != nil {
		log.Errorln(err)
	}
	count.UpdateFlag()
}
