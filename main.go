package main

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/soundtrack/middleware/beacon"
	"gitlab.com/soundtrack/middleware/server"
	"io/ioutil"
	"os"
)

func main() {
	// save beacon into database
	raw, _ := ioutil.ReadFile("./beacon.json")
	beacons := make([]beacon.Beacon, 0)
	json.Unmarshal(raw, &beacons)

	for _, beacon := range beacons {
		beacon.ToMongo()
	}
	log.Infoln("[OK] Beacons saved")

	// start server for handle beacon connection
	s := server.Server{
		Port: os.Getenv("SERVER_PORT"),
	}
	log.Infoln("[OK] Server is waiting for data")
	s.Start()
}
