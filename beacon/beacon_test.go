package beacon

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/soundtrack/middleware/position"
	"testing"
)

func TestCreateBeaconStruct(t *testing.T) {
	b := Beacon{
		Id: "123456789ABCDEF",
		Position: position.Position{
			X: 2.00,
			Y: 1.00,
		},
	}
	assert.Equal(t, b.Position.X, 2.00)
}

func TestCreateBeaconFromJson(t *testing.T) {
	_data := []byte(`
		{
    	"id": "123456789ABCDEF",
			"Position": {
				"X": 5.00,
				"Y": 6.00
			 }
    }
  `)
	b := NewBeacon(_data)
	assert.Equal(t, b.Position.X, 5.00)
}

func TestCreateJsonFromBeaconObject(t *testing.T) {
	_data := []byte(`
		{
    	"id": "123456789ABCDEF",
			"Position": {
				"X": 5.00,
				"Y": 6.00
			 }
    }
  `)
	b := NewBeacon(_data)
	assert.Equal(t, b.toJson(), `{"id":"123456789ABCDEF","Position":{"X":5,"Y":6}}`)
}
