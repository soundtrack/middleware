package beacon

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/soundtrack/middleware/position"
	"gopkg.in/mgo.v2"
	"os"
)

type Beacon struct {
	Id       string `json:"id"`
	Position position.Position
}

func NewBeacon(data []byte) *Beacon {
	var beacon Beacon
	json.Unmarshal(data, &beacon)
	return &beacon
}

func (self *Beacon) toJson() string {
	bytes, err := json.Marshal(self)
	if err != nil {
		log.Errorln(err.Error())
		os.Exit(1)
	}
	return string(bytes)
}

func (self *Beacon) toString() string {
	return self.toJson()
}

func (self *Beacon) ToMongo() {
	session, err := mgo.Dial("database")
	if err != nil {
		log.Errorln(err)
	}
	defer session.Close()
	c := session.DB("middleware").C("beacon")
	err = c.Insert(self)
	if err != nil {
		log.Errorln(err)
	}
}
