FROM golang:1.9.2-alpine3.6 as builder
RUN apk update && apk add git
WORKDIR /go/src/gitlab.com/soundtrack/middleware
COPY . ./
RUN go get . && GOOS=linux GOARCH=amd64 go build -o main

FROM alpine:3.6
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/soundtrack/middleware/main .
COPY beacon.json .
RUN chmod +x main
CMD ["./main"]
