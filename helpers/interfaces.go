package helpers

import "gopkg.in/mgo.v2"

/* MongoDB interface */

type Session interface {
	DB(name string) DataLayer
	Close()
}

type DataLayer interface {
	C(name string) Collection
}

type Collection interface {
	Find(query interface{}) Query
}

// MongoSession is currently a Mongo session.
type MongoSession struct {
	*mgo.Session
}

// DB shadows *mgo.DB to returns a DataLayer interface instead of *mgo.Database.
func (s MongoSession) DB(name string) DataLayer {
	return &MongoDatabase{Database: s.Session.DB(name)}
}

type MongoDatabase struct {
	*mgo.Database
}

func (d MongoDatabase) C(name string) Collection {
	return &MongoCollection{Collection: d.Database.C(name)}
}

type MongoCollection struct {
	*mgo.Collection
}

// Find shadows *mgo.Collection to returns a Query interface instead of *mgo.Query.
func (c MongoCollection) Find(query interface{}) Query {
	return MongoQuery{Query: c.Collection.Find(query)}
}

type MongoQuery struct {
	*mgo.Query
}

// Query is an interface to access to the database struct
type Query interface {
	All(result interface{}) error
}
