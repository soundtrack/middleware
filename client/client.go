package client

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/soundtrack/middleware/count"
	"gitlab.com/soundtrack/middleware/maths"
	"gitlab.com/soundtrack/middleware/stream"
	"net"
	"os"
)

type Client struct {
	Conn       net.Conn
	Connection *Client
}

func NewClient(connection net.Conn) *Client {
	client := &Client{
		Conn: connection,
	}
	log.Infoln("Client created, waiting for reception")
	go client.handleServerConnection()
	return client
}

func (connection *Client) handleServerConnection() {
	// we create a decoder that reads directly from the socket
	var s stream.Stream
	decoder := json.NewDecoder(connection.Conn)
	decoder.Decode(&s)
	connection.Conn.Close()
	log.Infoln("Stream received from client", s)
	s.ToMongo()
	count.UpdateFlag()
	if count.IsFull() {
		maths.GetEventPosition()
		os.Exit(0)
	}
}
